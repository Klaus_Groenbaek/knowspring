package dk.lundogbendsen.examples.knowspring.services;

import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    @Transactional
    public User getUser(String ip) {
        Optional<User> optional = repository.findUserByIp(ip);
        return optional.orElseGet(() -> repository.save(new User().setIp(ip)));
    }
}

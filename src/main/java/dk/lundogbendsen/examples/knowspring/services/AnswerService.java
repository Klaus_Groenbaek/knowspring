package dk.lundogbendsen.examples.knowspring.services;

import dk.lundogbendsen.examples.knowspring.controllers.QuestionAndAnswerBean;
import dk.lundogbendsen.examples.knowspring.controllers.QuestionFormBean;
import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.AnswerRepository;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.QuestionRepository;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void save(QuestionFormBean formBean, String ip) {

        Optional<User> optional = userRepository.findUserByIp(ip);
        User user = optional.orElseThrow(() -> new IllegalStateException("User does not exists"));

        for (QuestionAndAnswerBean bean : formBean.getRows()) {
            Question question = questionRepository.findOne(bean.getQuestionId());
            Answer answer = answerRepository.findByQuestionAndUser(question, user)
                    .orElse(new Answer().setQuestion(question).setUser(user));
            answer.setAnswer(bean.getAnswer());
            answerRepository.save(answer);
        }
    }
}

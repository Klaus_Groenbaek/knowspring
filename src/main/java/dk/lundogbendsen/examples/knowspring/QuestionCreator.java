package dk.lundogbendsen.examples.knowspring;


import com.google.common.collect.Lists;
import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.QuestionRepository;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.*;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class QuestionCreator {

    private static List<Class<?>> annotations = Lists.newArrayList(Autowired.class, Configuration.class, Bean.class, ComponentScan.class, Lazy.class, Scope.class, Component.class,  Repository.class,
            Controller.class, RestController.class, RequestMapping.class, ResponseBody.class, ResponseBody.class, RequestParam.class, PathParam.class,
            EnableWebMvc.class, ModelAttribute.class, SessionAttribute.class,  RequestAttribute.class, ControllerAdvice.class,
            PersistenceUnit.class, PersistenceContext.class, Transactional.class, EnableJpaRepositories.class, EnableTransactionManagement.class,
            EnableMBeanExport.class, ManagedAttribute.class, ManagedResource.class,
            Value.class, Profile.class, PropertySource.class, Valid.class,
            PostConstruct.class, PreDestroy.class, Primary.class);

    private static List<Class<?>> interfaces = Lists.newArrayList(InitializingBean.class, DisposableBean.class, Lifecycle.class,
            SmartLifecycle.class, ApplicationContextAware.class, ApplicationEventPublisherAware.class, BeanPostProcessor.class);

    private static List<Class<?>> abstractClasses = Lists.newArrayList(AbstractAnnotationConfigDispatcherServletInitializer.class, WebMvcConfigurationSupport.class, WebSecurityConfigurerAdapter.class);



    @Autowired
    private QuestionRepository questionRepository;

    @PostConstruct
    @Transactional
    public void createQuestions() {

     //   saveIfNotExists(annotations.stream().map(a -> new Question().setQuestion("Annotation: @" + a.getSimpleName())).collect(Collectors.toList()));
     //   saveIfNotExists(interfaces.stream().map(a -> new Question().setQuestion("Interface: " + a.getSimpleName())).collect(Collectors.toList()));
        saveIfNotExists(abstractClasses.stream().map(a -> new Question().setQuestion("Class: " + a.getSimpleName())).collect(Collectors.toList()));
    }

    private void saveIfNotExists(List<Question> annotationList) {
        for (Question question : annotationList) {
            Optional<Question> optional = questionRepository.findByQuestion(question.getQuestion());
            if (!optional.isPresent()) {
                questionRepository.save(question);
            }
        }

    }


}

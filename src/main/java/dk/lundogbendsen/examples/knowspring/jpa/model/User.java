package dk.lundogbendsen.examples.knowspring.jpa.model;


import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Collection;

@Accessors(chain = true)
@Setter
@Getter
@Entity
@Table(name = "PERSON") // user is often a restricted name
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String ip;

    @OneToMany(mappedBy = "user")
    private Collection<Answer> answers;

}

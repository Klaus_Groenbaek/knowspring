package dk.lundogbendsen.examples.knowspring.jpa.repositories;

import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface QuestionRepository extends JpaRepository<Question, Integer> {

    Optional<Question> findByQuestion(String question);
}

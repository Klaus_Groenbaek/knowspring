package dk.lundogbendsen.examples.knowspring.jpa.repositories;

import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findUserByIp(String ip);
}

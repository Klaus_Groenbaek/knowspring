package dk.lundogbendsen.examples.knowspring.jpa.repositories;

import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AnswerRepository extends JpaRepository<Answer, Integer>  {

    Optional<Answer> findByQuestionAndUser(Question q, User u);
}

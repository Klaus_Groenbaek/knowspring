package dk.lundogbendsen.examples.knowspring.jpa.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Accessors(chain = true)
@Setter
@Getter
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"QUESTION_ID", "USER_ID"}))
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @JoinColumn(name = "QUESTION_ID")
    @ManyToOne(optional = false)
    private Question question;

    @JoinColumn(name ="USER_ID")
    @ManyToOne(optional = false)
    private User user;

    @Column(nullable = false)
    private int answer;
}

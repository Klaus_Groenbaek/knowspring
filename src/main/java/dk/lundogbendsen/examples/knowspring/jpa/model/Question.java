package dk.lundogbendsen.examples.knowspring.jpa.model;

import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Collection;

@Accessors(chain = true)
@Setter
@Getter
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String question;

    @OneToMany(mappedBy = "question")
    private Collection<Answer> answers;
}

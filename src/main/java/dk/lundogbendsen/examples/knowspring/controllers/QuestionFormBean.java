package dk.lundogbendsen.examples.knowspring.controllers;

import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class QuestionFormBean {
    private User user;
    @Valid
    private List<QuestionAndAnswerBean> rows;
}

package dk.lundogbendsen.examples.knowspring.controllers;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

@Data
@Accessors(chain = true)
public class ResultBean {
    private String question;
    private int questionId;
    private double answerAverage;
}

package dk.lundogbendsen.examples.knowspring.controllers;

import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.QuestionRepository;
import dk.lundogbendsen.examples.knowspring.jpa.model.User;
import dk.lundogbendsen.examples.knowspring.services.AnswerService;
import dk.lundogbendsen.examples.knowspring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class QuizController {

    @Autowired
    private UserService service;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerService answerService;

    @ModelAttribute()
    public void configureModel(Model model, HttpServletRequest request) {
        QuestionFormBean formBean = new QuestionFormBean();
        String ip = request.getRemoteAddr();
        User user = service.getUser(ip);
        formBean.setUser(user);
        List<QuestionAndAnswerBean> rows = questionRepository.findAll().stream()
                .map(q -> new QuestionAndAnswerBean().setQuestion(q.getQuestion()).setQuestionId(q.getId()))
                .collect(Collectors.toList());

        for (Answer answer : user.getAnswers()) {
            for (QuestionAndAnswerBean row : rows) {
                if (row.getQuestionId() == answer.getQuestion().getId()) {
                    row.setAnswer(answer.getAnswer());
                }
            }
        }

        formBean.setRows(rows);
        model.addAttribute("form", formBean);
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @PostMapping()
    public String post(@Valid @ModelAttribute("form") QuestionFormBean formBean, HttpServletRequest request) {

        String ip = request.getRemoteAddr();
        answerService.save(formBean, ip);
        return "redirect:/";
    }



}

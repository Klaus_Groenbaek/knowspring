package dk.lundogbendsen.examples.knowspring.controllers;

import dk.lundogbendsen.examples.knowspring.jpa.model.Answer;
import dk.lundogbendsen.examples.knowspring.jpa.model.Question;
import dk.lundogbendsen.examples.knowspring.jpa.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.function.DoubleSupplier;

@Controller
public class ResultController {


    private final QuestionRepository questionRepository;

    @Autowired
    public ResultController(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @GetMapping("results")
    public ModelAndView results() {

        List<ResultBean> rows = new ArrayList<>();

        List<Question> questions = questionRepository.findAll();
        for (Question question : questions) {
            OptionalDouble average = question.getAnswers().stream().mapToInt(Answer::getAnswer).average();
            rows.add(new ResultBean().setQuestion(question.getQuestion()).setAnswerAverage(average.orElseGet(() -> 0)));
        }

        ModelAndView result = new ModelAndView("result");
        result.addObject("rows", rows);
        return result;


    }

}

package dk.lundogbendsen.examples.knowspring.controllers;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;

@Data
@Accessors(chain = true)
public class QuestionAndAnswerBean {
    private String question;
    private int questionId;
    @Range(min = 1, max = 5)
    private int answer;
}

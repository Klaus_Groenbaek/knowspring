package com.lundogbendsen.examples.knowspring;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.Metamodel;
import javax.sql.DataSource;
import java.util.Properties;

import static org.eclipse.persistence.config.PersistenceUnitProperties.*;

/**
 * Simple main that creates a database from the current JPA structure, to make it easier to create new flyway scripts
 * @author Klaus Groenbaek
 *         Created 06/09/15.
 */
@Configuration
@EnableJpaRepositories(basePackages = {"dk.lundogbendsen.examples.knowspring"})
@EnableTransactionManagement
public class CreateScripts {


    private JpaVendorAdapter jpaVendorAdapter() {
        EclipseLinkJpaVendorAdapter jpaVendorAdapter = new EclipseLinkJpaVendorAdapter();
        jpaVendorAdapter.setDatabase(Database.MYSQL);
        return jpaVendorAdapter;
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }


    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("eclipselink.logging.parameters", "true");
        jpaProperties.setProperty(WEAVING, "false");
        jpaProperties.setProperty(CACHE_SHARED_DEFAULT, "false");

        jpaProperties.put(DDL_GENERATION, PersistenceUnitProperties.DROP_AND_CREATE);
        jpaProperties.put(CREATE_JDBC_DDL_FILE, "create.sql");
        jpaProperties.put(DROP_JDBC_DDL_FILE, "drop.sql");
        jpaProperties.put(DDL_GENERATION_MODE, DDL_SQL_SCRIPT_GENERATION);

//        jpaProperties.put(LOGGING_FILE, "output.log");
//        jpaProperties.put(LOGGING_LEVEL, "FINE");

        LocalContainerEntityManagerFactoryBean lemfb = new LocalContainerEntityManagerFactoryBean();
        lemfb.setDataSource(dataSource);
        lemfb.setJpaVendorAdapter(jpaVendorAdapter());
        lemfb.setJpaProperties(jpaProperties);
        lemfb.setPackagesToScan("dk.lundogbendsen.examples.knowspring");
        return lemfb;
    }

    @Bean()
    public DataSource dataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser("knowspring");
        dataSource.setPassword("knowspring");
        dataSource.setDatabaseName("knowspring");
        return dataSource;
    }



    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(CreateScripts.class);
        context.refresh();

        EntityManagerFactory emf = context.getBean(EntityManagerFactory.class);
        EntityManager em = emf.createEntityManager();
        try {
            Metamodel metamodel = em.getMetamodel();
            System.out.println("metamodel = " + metamodel);
        } finally {
            em.close();
        }


    }

}
